package pl.catchycode.roboguicetutorial;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.inject.Inject;

public class ExampleFragment extends RoboFragment{
	@InjectView(R.id.multiply_button) Button mMultiplyButton;
	@Inject SimpleMultipler mMultipler;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_layout, container, false);
		return v;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		mMultiplyButton.setText("Liczba " + mMultipler.getCounter());
		mMultiplyButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mMultiplyButton.setText("Liczba " + mMultipler.multiply());
			}
		});
	}
	
}
