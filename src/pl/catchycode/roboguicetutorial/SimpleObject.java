package pl.catchycode.roboguicetutorial;

public class SimpleObject {
	
	private Integer mSimpleInt;
	
	public SimpleObject() {
	}

	public Integer getSimpleInt() {
		return mSimpleInt;
	}
	
	public void setSimpleInt(Integer mSimpleInt) {
		this.mSimpleInt = mSimpleInt;
	}
}
