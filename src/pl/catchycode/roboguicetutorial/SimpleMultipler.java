package pl.catchycode.roboguicetutorial;

import roboguice.inject.ContextSingleton;

@ContextSingleton
public class SimpleMultipler {
	
	private int counter = 1;

	public int multiply() {
		counter*=2;
		if (counter>2048)
			counter =1;
		return counter;
	}
	
	public int getCounter() {
		return counter;
	}
	
}
