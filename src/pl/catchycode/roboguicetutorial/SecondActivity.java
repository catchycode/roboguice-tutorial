package pl.catchycode.roboguicetutorial;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import android.os.Bundle;
import android.util.Log;

import com.google.inject.Inject;

@ContentView(R.layout.second_layout)
public class SecondActivity extends RoboFragmentActivity {
	@Inject SimpleObject mSimpleObject;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mSimpleObject.setSimpleInt(10);
		Log.i("CATCHYCODE", mSimpleObject.getSimpleInt().toString());
		
		getSupportFragmentManager().beginTransaction()
			.replace(R.id.container, new ExampleFragment()).commit();
		
	}

}
