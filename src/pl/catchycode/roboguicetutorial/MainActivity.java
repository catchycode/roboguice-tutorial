package pl.catchycode.roboguicetutorial;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectResource;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

@ContentView(R.layout.main_layout)
public class MainActivity extends RoboActivity {
	@InjectView(R.id.example_button) Button mExampleButton;
	@InjectView(R.id.example_image_view) ImageView mExampleImageView;
	@InjectView(R.id.example_text_view) TextView mExampleTextView;
	@InjectView(R.id.second_activity) Button mSecondActivityButton;
	
	@InjectResource(R.color.super_color) Integer mSuperColor;
	@InjectResource(R.string.super_text) String mSuperText;
	@InjectResource(R.drawable.super_image) Drawable mSuperDrawable;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Use color to set button background
		mExampleButton.setBackgroundColor(mSuperColor);
		
		// Use string to set new text
		mExampleTextView.setText(mSuperText);
		
		// Use image when button clicked
		mExampleButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mExampleImageView.setImageDrawable(mSuperDrawable);
			}
		});
		
		// Use to show second activity
		mSecondActivityButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, SecondActivity.class);
				startActivity(intent);
			}
		});
	}

}
